<?php

class TwoSum {
    function calculate($nums, $target) {
        foreach ($nums as $key => $value) {
            unset($nums[$key]);
            if (($key2 = array_search($target - $value, $nums))) {
                return [$key, $key2];
            }
        }
        return [];
    }
}

$twosum = new TwoSum;
$numsGet = readline("input number array separate with ',' without space, ex:2,7,11,15 = ");
$target = readline("input target = ");
$nums = explode(",",$numsGet);

if(count($nums) < 2){
    echo "Result not found";
}else{
    $run = $twosum->calculate($nums, $target);

    if($run){
        foreach($run as $key => $value){
            $result[] = $value;
        }

        $str = implode (", ", $result);
        echo "[".$str."]\n";
    }else{
        echo "Result not found";
    }
}
